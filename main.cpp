#define _CRT_SECURE_NO_WARNINGS
#include <bits/stdc++.h>
#include <Windows.h>;
using std::vector; using std::pair; typedef pair<int, int> pii; typedef pair<double, double> pdd;
/*
점 데이터를 입력받고, 이중 K개(현재는 2개)를 선택하여
각각을 클러스터의 중심점 위치로 선정한다.

이후 모든 점에대해 어느 클러스터에 속하는지 분류를 한뒤에
분류가 끝난뒤 각 클러스터 그룹내의 중심점위치를 다시 정한다.
이때 중심점위치를 다시정하면서 각각의 점들이 다른 클러스터로 변화하기도하는데,

이러면 다시 중심점위치를 계산, 다시 모든 점을 재분류하는 과정을 반복한다.
여기서 모든 점이 클러스터의 중심점이 바뀐뒤에서 같은 클러스터에 속한다면
중단한다.
*/

vector<pii> data;
vector<int> cluster;
int graph[11][11];
int size, cnt; //데이터의 총갯수, 중심점 계산횟수
pdd m1, m2, m3; //중심점 위치
void func();
void visualization();
bool calculate();
void makeGraph();
void classification();
void init();

void init() {
    while (1) {
        int x, y;
        printf("S%d 의 x, y(0= < x,y <= 10)값을 띄어쓰기로 입력하세요. (x,y < 0 입력시 종료):\n", size);
        scanf("%d%d", &x, &y);

        if (x < 0 || y < 0) break;
        data.push_back({ x, y });
        cluster.push_back(0);
        size++;
    }

    //처음 중심점위치는 데이터중 임의로 골라서 정한다.
    int temp1;
    while (1) {
        printf("초기센터(m1)로 사용할 Si의 i값을 입력하세요. (0 <= i < %d):\n", size);
        scanf("%d", &temp1);
        if (0 <= temp1 && temp1 < size) break; //정상값이어야 다음단계진행
    }
    m1 = { data[temp1].first , data[temp1].second };

    int temp2;
    while (1) {
        printf("초기센터(m2)로 사용할 Si의 i값을 입력하세요. (0 <= i < %d):\n", size);
        scanf("%d", &temp2);
        if (0 <= temp2 && temp2 < size && temp1 != temp2) break;
    }
    m2 = { data[temp2].first , data[temp2].second };

    int temp3;
    while (1) {
        printf("초기센터(m3)로 사용할 Si의 i값을 입력하세요. (0 <= i < %d):\n", size);
        scanf("%d", &temp3);
        if (0 <= temp3 && temp3 < size && temp1 != temp2 && temp2 != temp3) break;
    }
    m3 = { data[temp3].first , data[temp3].second };

    //초기분류 실행
    classification();

    makeGraph();
}

void func() {
    while (1) {
        visualization();
        if (calculate() || cnt >= 50) break;

        classification();
        makeGraph();
    }
    printf("\n\n>>>>>>>>>>>>>>>>>>> END\n\n\n\n\n");
}

//중심점을 다시 계산
bool calculate() {
    pdd prev_m1 = m1, prev_m2 = m2, prev_m3 = m3;
    int m1_sum_x = 0;
    int m1_sum_y = 0;
    int m2_sum_x = 0;
    int m2_sum_y = 0;
    int m3_sum_x = 0;
    int m3_sum_y = 0;
    int m1_cnt = 0;
    int m2_cnt = 0;
    int m3_cnt = 0;
    for (int i = 0; i < size; i++) {
        if (cluster[i] == 1) {
            m1_sum_x += data[i].first;
            m1_sum_y += data[i].second;
            m1_cnt++;
        }
        else if (cluster[i] == 2) {
            m2_sum_x += data[i].first;
            m2_sum_y += data[i].second;
            m2_cnt++;
        }
        else {
            m3_sum_x += data[i].first;
            m3_sum_y += data[i].second;
            m3_cnt++;
        }
    }
    m1 = { m1_sum_x / (double)m1_cnt, m1_sum_y / (double)m1_cnt };
    m2 = { m2_sum_x / (double)m2_cnt, m2_sum_y / (double)m2_cnt };
    m3 = { m3_sum_x / (double)m3_cnt, m3_sum_y / (double)m3_cnt };
    cnt++;
    
    if (prev_m1 == m1 && prev_m2 == m2 && prev_m3 == m3)
        return true;
    else
        return false;
}

//그래프를 시각화하고 m1, m2값을 출력
void visualization() {
    for (int i = 10; i >= 0; i--) {
        for (int j = 0; j < 11; j++) {
            if (graph[i][j] == 0) {
                printf("  ");
            }
            else if(graph[i][j] == 1) {
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED);
                printf("●");
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            }
            else if(graph[i][j] == 2) {
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE);
                printf("●");
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            }
            else {
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
                printf("●");
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            }
        }
        printf("\n");
    }
    printf("m1 = (%lf,%lf)\nm2 = (%lf,%lf)\nm3 = (%lf,%lf)\n",m1.first, m1.second, m2.first, m2.second, m3.first, m3.second);

}

//실제로 그래프를 데이터화
void makeGraph() {
    for (int i = 0; i < 11; i++)
        for (int j = 0; j < 11; j++)
            graph[i][j] = 0; //초기화

    for (int i = 0; i < size; i++) {
        int x = data[i].first;
        int y = data[i].second;
        cluster[i] == 1 ? graph[x][y] = 1 : cluster[i] == 2 ? graph[x][y] = 2 : graph[x][y] = 3;
    }
}

//모든점을 어느 클러스터에 속하는지 분류
void classification() {
    double d1, d2, d3;
    
    for (int i = 0; i < size; i++) {
        d1 = sqrt(pow((m1.first - data[i].first), 2) + pow((m1.second - data[i].second), 2));
        d2 = sqrt(pow((m2.first - data[i].first), 2) + pow((m2.second - data[i].second), 2));
        d3 = sqrt(pow((m3.first - data[i].first), 2) + pow((m3.second - data[i].second), 2));
        if (d1 > d2)
            d2 > d3 ? cluster[i] = 3 : cluster[i] = 2;
        else
            d1 > d3 ? cluster[i] = 3 : cluster[i] = 1;
    }       
    
}

int main(void) {
    init();
    func();


    return 0;
}